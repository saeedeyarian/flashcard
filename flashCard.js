let button = document.querySelector (".header section");
let saveClose = document.querySelector(".save-close-btn");
let output = document.querySelector(".flashcard-output");
let inputQ = document.querySelector (".input-question");
let inputA = document.querySelector (".input-answer");
let container = document.querySelector(".flashcards-container");

button.addEventListener ("click", (event) => {
    const item =event.target
    if (item.classList[0] === "add-card-btn") {
        container.style.display = "block"
    } else {
        output.innerHTML = "";
    }
});
saveClose.addEventListener ("click", (event) => {
    const data = event.target
    if (data.classList[0] === "save-card-btn") {
        const div = document.createElement ("div");
        const divData = `
                        <div class="question-text">${inputQ.value}</div>
                        <div class="answer-text">${inputA.value}</div>`
        div.innerHTML = divData;
        output.appendChild (div);
    } else {
        container.style.display = "none";
    }
    inputQ.value = "";
    inputA.value = "";
});